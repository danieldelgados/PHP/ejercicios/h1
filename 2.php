<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $entero=10;
            $cadena="hola";
            $real=23.6;
            $logico=TRUE;
            
            //var_dump nos imprime el valor y el tipo de la variable
            var_dump($entero); //salida variable tipo entero
            var_dump($cadena);//salida variable tipo string 'hola' length=4
            var_dump($real);//salida variable tipo float
            var_dump($logico);//salida variable boolean true en este caso al estar declarada la variable en true.
            
            $logico=(int)$logico;// aqui cambiamos el tipo de dato de boolean a entero
            $entero=(float)$entero;//aqui el entero pasa a ser de tipo float
            settype($logico, "int");// aqui cambiamos el tipo de dato de boolean a entero
            
            var_dump($entero); 
            var_dump($cadena);
            var_dump($real);
            var_dump($logico);
            
        ?>
    </body>
</html>
