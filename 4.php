<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $s;
            $j;
            $k;
            $i;
            $a=2.345;
            
            $s=0;
            for($j=1;$j<=4;$j++){
                echo $j;// con el for loop imprimirá desde el 1 al 4
                if($j%2==0){// aquí le decimos que los numeros pares
                    $s+=$j;// se almacenaran en la variable s
                }
            }
            
            printf("<br> %d", $j);// aquí la funcion printf nos indica que la variable se imprimirá en formato entero 
            var_dump($s);// aqui mostramos la suma de los numeros pares de la if clause 2+4
            $i=10;
            while($i>0){
                $i=$i-1;
            }
            echo $i;// aqui nos muestra el resultado del while loop el cual es cero
            echo gettype($i) . "<br>"; // nos dice el tipo de variable en este caso int entero
            print_r($i);//esta función nos imprime informacion entendible por humanos es decir solo nos imprime el valor
            var_dump($a);// esta funcion nos da informacion mas detallada incluyendo el tipo, el valor y la ruta de la variable
        ?>
    </body>
</html>
