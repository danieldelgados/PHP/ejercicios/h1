<?php
if(!empty($_REQUEST)){
    if(((int)$_REQUEST["numero"]) > 0){
        $caso="bien";
    }else{
        $caso='mal';
    }
}else{// este es el primer caso proque el el elemento nombre viene vacio
    $caso='mal';
}
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
/*            otra forma de seleccionar una etiqueta en este caso por su type*/
            input[type="number"]{
                width:100px;
            }
/*            pseudo selector ::before coloca lo que quereamos antes del contenido y con ::after despues */
            .obligatorio::before{
                content:"Obligatorio";
                min-width: 150px;
                display: inline-block;
            }
            .noObligatorio::before{
                content: "opcional";
                min-width: 150px;
                display: inline-block;
            }
        </style>
    </head>
    <body>
        <?php
            if($caso=="bien"){
                var_dump($_REQUEST);
            }else{
        ?>
        <div>
            <form name="f">
              
                <div class="obligatorio"><input required placeholder="Introduce un número" step="1" min="1" max="100" type="number" name="numero"></div>
                <div class="noObligatorio"><input placeholder="Introduce otro número" step="1" min="1" max="100" type="number" name="numero1"></div> 
                <input type="submit" value="Enviar" name="boton">
              
                
            </form>
            
        </div>
        <?php 
        
            }        
        ?>
        
    </body>
</html>